# camsend

Send a local video (webcam) device to another computer as a remote video device, so remote applications can use it.

( We're actually having issues with chromium )

This will require v4l2loopback module on the remote device: https://github.com/umlaeute/v4l2loopback

For Debian:

    sudo apt install v4l2loopback-dkms
    sudo modprobe v4l2loopback

A port exists for FreeBSD: http://video4bsd.sourceforge.net/ (untested)

## Usage

Once the remote video device is created using `v4l2loopback`, use

    camsend [user@]host

to send the local webcam.

Please consider using `~/.ssh/config` to adjust your ssh parameters like
username and port, note that `ssh -oCompression=no` is used in the script.

You might want to tweak the encoding and decoding parameters directly in the
script (on both sides) until you get smooth results.

    ffmpeg [...]
        -speed 12 \
        -threads 8 \
        -r 24 \
        -b:v 6000k

## Authors

This script was written by Nex and XS.
